# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import wireguard with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}


{% for vpn, data in salt.pillar.get('wireguard', None).items() %}
wireguard_server_config_{{ vpn }}_client_managed:
  file.managed:
    - name: /etc/wireguard/{{ vpn }}.conf
    - source: salt://wireguard/files/default/config.conf.jinja
    - mode: 600
    - user: root
    - group: root
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_package_install }}
    - context:
        client: {{ data | json }}
        listen_port: {{ data.listen_port | json }}
        dns_server: {{ data.dns_server | json }}
        server_address: {{ data.server_address | json }}
        server_public_key: {{ data.server_public_key | json }}
{% endfor %}
