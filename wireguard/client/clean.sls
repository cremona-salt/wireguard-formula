{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import wireguard with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set interface_name = wireguard.server_address|replace('.', '_') } }}

wireguard_server_config_removed:
  file.absent:
    - name: /etc/wireguard/{{ wireguard.server_address|replace('.', '_') }}.conf

wireguard_server_wireguard_quick_service_stopped:
  service.dead:
    - name: wg-quick@{{ interface_name }}
    - enable: False
