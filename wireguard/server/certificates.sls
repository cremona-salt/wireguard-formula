# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import wireguard with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

wireguard_server_private_key_created:
  file.managed:
    - name: /etc/wireguard/privatekey
    - user: root
    - group: root
    - mode: 600
    - contents: {{ wireguard.server.private_key }}
    - require:
      - sls: {{ sls_package_install }}

wireguard_server_public_key_created:
  file.managed:
    - name: /etc/wireguard/publickey
    - user: root
    - group: root
    - mode: 600
    - contents: {{ wireguard.server.public_key }}
    - require:
      - sls: {{ sls_package_install }}
      - wireguard_server_private_key_created
