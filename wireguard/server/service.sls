# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- set sls_server_config = tplroot ~ '.server.config' %}
{%- from tplroot ~ "/map.jinja" import wireguard with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set interface_name = wireguard.server.interface_name %}
include:
  - {{ sls_package_install }}
  - {{ sls_server_config }}

wireguard_server_wireguard_quick_service_managed:
  service.running:
    - name: wg-quick@wg0
    - enable: True
    - watch:
      - file: wireguard_server_config_managed
