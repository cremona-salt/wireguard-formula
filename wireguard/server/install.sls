# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import wireguard with context %}

wireguard_server_qrencode_package_installed:
  pkg.installed:
    - name: {{ wireguard.qrencode.pkg.name }}
