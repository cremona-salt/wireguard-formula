# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import wireguard with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{%- set interface_name = wireguard.server.interface_name %}
wireguard_server_config_managed:
  file.managed:
    - name: {{ wireguard.config }}
    - source: salt://wireguard/files/default/server-wg0.conf.jinja
    - mode: 600
    - user: root
    - group: root
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_package_install }}
    - context:
        server: {{ wireguard.server | json }}
        listen_port: {{ wireguard.listen_port | json }}
        clients: {{ wireguard.clients | json }}

wireguard_server_ip_forwarding_enabled:
  sysctl.present:
    - name: net.ipv4.ip_forward
    - value: 1

{% for client, data in wireguard.clients.items() %}
{% if data.create_server_config == True %}
wireguard_server_config_{{ client }}_client_managed:
  file.managed:
    - name: /etc/wireguard/{{ client }}.conf
    - source: salt://wireguard/files/default/config.conf.jinja
    - mode: 600
    - user: root
    - group: root
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_package_install }}
    - context:
        client: {{ data | json }}
        server_public_key: {{ wireguard.server.public_key | json }}
        listen_port: {{ wireguard.listen_port | json }}
        dns_server: {{ wireguard.dns_server | json }}
        server_address: {{ wireguard.server_address | json }}
{% endif %}
{% endfor %}
