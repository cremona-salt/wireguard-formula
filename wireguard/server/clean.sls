{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import wireguard with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

wireguard_server_private_key_removed:
  file.absent:
    - name: /etc/wireguard/privatekey

wireguard_server_public_key_removed:
  file.absent:
    - name: /etc/wireguard/publickey

wireguard_server_config_removed:
  file.absent:
    - name: {{ wireguard.config }}

wireguard_server_ip_forwarding_disabled:
  sysctl.present:
    - name: net.ipv4.ip_forward
    - value: 0

wireguard_server_wireguard_quick_service_stopped:
  service.dead:
    - name: wg-quick@wg0
    - enable: False

wireguard_server_qrencode_package_removed:
  pkg.removed:
    - name: {{ wireguard.qrencode.pkg.name }}
