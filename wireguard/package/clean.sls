# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{#- {%- set sls_config_clean = tplroot ~ '.config.clean' %} #}
{%- from tplroot ~ "/map.jinja" import template with context %}

{#-
include:
  - {{ sls_config_clean }}
#}

{% set os_family = salt.grains.get('os_family', None) %}

{% if os_family == 'Debian' %}
wireguard_server_package_ppa_removed:
  pkgrepo.absent:
    - ppa: wireguard/wireguard
{% endif %}

wireguard_server_package_installed:
  pkg.removed:
    - name: {{ wireguard.pkg.name }}
{#- - require:
      - sls: {{ sls_config_clean }}
      #}
