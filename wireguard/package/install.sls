# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import wireguard with context %}

{% set os_family = salt.grains.get('os_family', None) %}

{#
{% if os_family == 'Debian' %}
wireguard_package_ppa_managed:
  pkgrepo.managed:
    - ppa: wireguard/wireguard
{% endif %}
#}

wireguard_package_directory_managed:
  file.directory:
    - name: /etc/wireguard
    - user: root
    - group: root

wireguard_package_installed:
  pkg.installed:
    - name: {{ wireguard.pkg.name }}
    {#
    {% if os_family == 'Debian' %}
    - require:
      - wireguard_package_ppa_managed
    {% endif %}
    #}

{#
wireguard_package_module_loaded:
  kmod.present:
    - name: {{ wireguard.module.name }}
    #}
