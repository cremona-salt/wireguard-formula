# Wireguard formula

## Configurations for mobile platforms

To set up a config for a mobile, generate them on the server and then store the
resulting config files for future use. There's no reliable place to store them
on client devices, and no way (afaik) to {,re}generate them when they get lost.

```bash
sudo -i
wg genkey | tee mobile_device_private.key | wg pubkey > mobile_device_public.key
```

Remove these keys once you've added them to pillar data.

Run a salt update to populate the file, then generate the QR code with the
following:

```bash
qrencode -t ansiutf8 < client_wireguard.conf
```
